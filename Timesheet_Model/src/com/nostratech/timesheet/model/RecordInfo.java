package com.nostratech.timesheet.model;

import java.io.PrintStream;
import java.io.PrintWriter;

import java.sql.Timestamp;

import java.util.HashMap;
import java.util.Map;

import oracle.adf.share.ADFContext;

import oracle.jbo.Key;
import oracle.jbo.server.EntityImpl;

public class RecordInfo {        
    
    private EntityId entityId;
    private Map<Object, Object> oldAttrs;
    private Map<Object, Object> newAttrs;
    private Action action;
    private Timestamp now;
    private String currentUser;
    
    public RecordInfo(int operation, String entityName, Key primaryKey) {
        action = new Action(operation);
        entityId = new EntityId(entityName, primaryKey);

        now = new Timestamp(System.currentTimeMillis());
        currentUser = ADFContext.getCurrent().getSecurityContext().getUserPrincipal().getName();

        oldAttrs = new HashMap<Object, Object>();
        newAttrs = new HashMap<Object, Object>();  
    }

    public EntityId getEntityId() {
        return entityId;
    }

    public Action getAction() {
        return action;
    }
    
    public String getUser() {
        return currentUser;
    }
    
    public Map<Object, Object> getOldAttributes() {
        return oldAttrs;
    }

    public Map<Object, Object> getNewAttributes() {
        return newAttrs;
    }
    
    public void addOldAttribute(Object key, Object value) {
        oldAttrs.put(key, value);
    }

    public void addNewAttribute(Object key, Object value) {
        newAttrs.put(key, value);
    }
    
    public void print(PrintWriter out) {
        out.println("*** Database records modified ***");
        out.println("User: " + getUser());
        out.println("Entity: " + getEntityId().entityName);
        out.println("Primary key: " + getEntityId().primaryKey);
        out.println("Operation: " + getAction());
        
        if (getAction().getOperation() == EntityImpl.DML_UPDATE) {
            printAttributes(out, false);
        }
        printAttributes(out, true);
    }

    private void printAttributes(PrintWriter out, boolean newAttr) {
        Map<Object, Object> attrs;
        if (newAttr) {
            attrs = getNewAttributes();
            out.println("New attributes: ");
        } else {
            attrs = getOldAttributes();
            out.println("Old attributes: ");
        }
        for (Map.Entry<Object, Object> attrib : attrs.entrySet()) {
            out.println("\t" + attrib.getKey() + " = " + attrib.getValue());
        }
    }

    private static class Action {
        private int operation;

        public Action(int status) {
            this.operation = status;
        }
        
        public int getOperation() {
            return operation;
        }

        public String toString() {
            switch (operation) {
            case EntityImpl.DML_INSERT:
                return "INSERT";
            case EntityImpl.DML_UPDATE:
                return "UPDATE";
            case EntityImpl.DML_DELETE:
                return "DELETE";
            default:
                return "???";
            }
        }
    }
    
    public static class EntityId {
        public String entityName;
        public Key primaryKey;
        
        public EntityId(String name, Key pk) {
            entityName = name;
            primaryKey = pk;
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (!(object instanceof EntityId)) {
                return false;
            }

            final EntityId other = (EntityId)object;
            if (entityName == null || other.entityName == null)
                return false;
            if (primaryKey == null || other.primaryKey == null)
                return false;

            return entityName.equals(other.entityName) &&
                primaryKey.equals(other.primaryKey);
        }

        @Override
        public int hashCode() {
            final int PRIME = 37;
            int result = 1;
            result =
                    PRIME * result + ((entityName == null) ? 0 : entityName.hashCode());
            result =
                    PRIME * result + ((primaryKey == null) ? 0 : primaryKey.hashCode());
            return result;
        }
    }
    
}
