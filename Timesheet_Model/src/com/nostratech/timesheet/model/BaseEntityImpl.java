package com.nostratech.timesheet.model;

import java.sql.Timestamp;

import oracle.adf.share.ADFContext;
import oracle.adf.share.logging.ADFLogger;
import oracle.adf.share.security.SecurityContext;

import oracle.jbo.AttributeDef;
import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;
import oracle.jbo.StructureDef;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.TransactionEvent;
import oracle.jbo.server.TransactionListener;
import oracle.jbo.server.TransactionPostListener;
import oracle.jbo.server.ViewRowImpl;

public class BaseEntityImpl extends EntityImpl {

    @Override
    protected void doDML(int operation, TransactionEvent transactionEvent) {
        super.doDML(operation, transactionEvent);
        
//        System.err.println("\n\nBaseEntityImpl doDML()");

       if (operation == DML_INSERT ||
           operation == DML_UPDATE ||
           operation == DML_DELETE) {

            RecordInfo rec = new RecordInfo(operation, getEntityDef().getName(), getPrimaryKey());

            Object val = null;
            Object oldVal = null;
            String name = null;

            for (int i = 0; i < getEntityDef().getAttributeCount(); i++) {
                val = getAttribute(i);
                oldVal = getPostedAttribute(i);
                name =getEntityDef().getAttributeDef(i).getColumnName();

                if (isAttributeChanged(i)) {
                    rec.addOldAttribute(name, oldVal);
                    rec.addNewAttribute(name, val);
                }
            }

            Auditor.get().recordChanged(rec);
        }       
    }

    @Override
    public void afterCommit(TransactionEvent transactionEvent) {
        super.afterCommit(transactionEvent);

//        System.err.println("\n\nBaseEntityImpl afterCommit");

        Auditor.get().recordCommited(getEntityDef().getName(), getPrimaryKey());
    }

    @Override
    public void afterRollback(TransactionEvent transactionEvent) {
        super.afterRollback(transactionEvent);

//        System.err.println("\n\nBaseEntityImpl afterRollback");

        Auditor.get().recordRolledback(getEntityDef().getName(), getPrimaryKey());
    }
    
//    private String getAllAttributes() {
//        StructureDef def = getStructureDef();
//        StringBuilder sb = new StringBuilder();
//        int numAttrs = def.getAttributeCount();
//        AttributeDef[] attrDefs = def.getAttributeDefs();
//        for (int i = 0; i < numAttrs; i++) {
//            Object value = getAttribute(i);
//            sb.append(i > 0 ? "  " : "")
//                .append(attrDefs[i].getName())
//                .append("=").append(value == null ? "<null>" : value)
//                .append(i < numAttrs - 1 ? "\n" : "");
//        }
//        return sb.toString();
//    }
    
}
