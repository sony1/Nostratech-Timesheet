package com.nostratech.timesheet.model;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import java.util.HashMap;
import java.util.Map;

import oracle.jbo.Key;
import oracle.jbo.server.EntityImpl;

import org.apache.log4j.Logger;

public class Auditor {

    private final Logger LOGGER = Logger.getLogger(Auditor.class);
    
    private static Auditor instance;
    private Map<RecordInfo.EntityId, RecordInfo> records;
    
    public static synchronized Auditor get() {
        if (instance == null) {
            instance = new Auditor();
        }
        return instance;
    }
    
    private Auditor() {
        records = new HashMap<RecordInfo.EntityId, RecordInfo>();
    }

    public void recordChanged(RecordInfo rec) {
        records.put(rec.getEntityId(), rec);
        
//        System.err.println(String.format("Tracking entity changes: %s (%s)", rec.getEntityId().entityName, rec.getEntityId().primaryKey.toString()));
    }
    
    public void recordCommited(String entityName, Key primaryKey) {
        RecordInfo.EntityId id = new RecordInfo.EntityId(entityName, primaryKey);
        RecordInfo rec = records.get(id);
        
        /*
         * null terjadi pada saat dimana terdapat master table
         * dan yg mengalami perubahan hanya detail table saja
         */
        if (rec != null) {        
            // start logging
//            System.err.println("*** Entity Changed ***");
            StringWriter sw = new StringWriter();
            PrintWriter out = new PrintWriter(sw);
            rec.print(out);
            
            if (LOGGER.isInfoEnabled()) {
                LOGGER.info(sw.toString());
            }
            
            try {
                out.close();
                sw.close();
            } catch (IOException ex) { ex.printStackTrace(); }
            
            records.remove(id);
//            System.err.println(String.format("Record commited: %s (%s)",
//                                             rec.getEntityId().entityName,
//                                             rec.getEntityId().primaryKey.toString()));
        }
    }
    
    public void recordRolledback(String entityName, Key primaryKey) {
        RecordInfo.EntityId id =
            new RecordInfo.EntityId(entityName, primaryKey);
        records.remove(id);

//        System.err.println(String.format("Record rolledback: %s (%s)",
//                                         id.entityName,
//                                        id.primaryKey.toString()));
    }

}
