package com.nostratech.timesheet.model;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCDataControl;

import oracle.adf.share.ADFContext;

import oracle.binding.BindingContainer;

public class AdfUtils {

    /**
     * @return current AppModule instance
     */
    public static AppModuleImpl getAppModule() {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCDataControl dc =
            bindingContext.findDataControl("AppModuleDataControl");
        return (AppModuleImpl)dc.getDataProvider();
    }

    /**
     * @return current bindings
     */
    public static BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    /**
     * @return currently logged-in user name
     */
    public static String getCurrentUserLoginName() {
        return ADFContext.getCurrent().getSecurityContext().getUserPrincipal().getName();
    }
}
