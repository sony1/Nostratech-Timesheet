package com.nostratech.timesheet.model;

import com.nostratech.timesheet.model.vo.PersonalTimesheetVOImpl;

import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.faces.event.ActionEvent;

import oracle.adf.model.bean.DCDataRow;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.data.RichTree;
import oracle.adf.view.rich.component.rich.nav.RichCommandLink;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.model.ChildPropertyTreeModel;

import oracle.binding.BindingContainer;

import oracle.jbo.Row;
import oracle.jbo.ViewCriteria;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;

import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.model.TreeModel;

// http://yonaweb.be/creating_your_own_treemodel_adf_11g_0

public class TimesheetHistoryTree {
    private TreeItem root;
    private TreeModel model;
    private RichTable timesheetTable;

    public TimesheetHistoryTree() {
        root = new TreeItem("All", null);
        root.setChildren(new ArrayList<TreeItem>());
        root.getChildren().add(new TreeItem("This Week", "0"));
        root.getChildren().add(new TreeItem("Last Week", "1"));
        root.getChildren().add(new TreeItem("Last 2 Weeks", "2"));
        root.getChildren().add(new TreeItem("Last 3 Weeks", "3"));
        root.getChildren().add(new TreeItem("Last Month", "4"));
    }
    
    public TreeModel getModel(){
        if (model == null) {
            model = new ChildPropertyTreeModel(root, "children");
        }
        return model;
    }

    public void onTreeItemSelected(ActionEvent actionEvent) {        
        RichCommandLink link = (RichCommandLink)actionEvent.getSource();
        TreeItem item = getTreeItem(link.getText());
        PersonalTimesheetVOImpl vo =
            (PersonalTimesheetVOImpl)AdfUtils.getAppModule().findViewObject("PersonalTimesheetVO1");
        
        if (item != null) {
            int n = Integer.parseInt(item.getAction());
            int workDays = 5;
            
            Calendar c = mondayOfLastNWeek(n);
            c.add(Calendar.DAY_OF_MONTH, -1);   // SQL between/exclusive
            Date startDate = c.getTime();       
            c.add(Calendar.DAY_OF_MONTH, workDays + 1);
            Date endDate = c.getTime();
                        
            ViewCriteria vc = vo.getViewCriteria("PersonalTimesheetByDateCriteria");
            vo.setstartDateBind(new oracle.jbo.domain.Date(new java.sql.Timestamp(startDate.getTime())));
            vo.setendDateBind(new oracle.jbo.domain.Date(new java.sql.Timestamp(endDate.getTime())));
            vo.applyViewCriteria(vc);            
        }
        
        vo.removeApplyViewCriteriaName("PersonalTimesheetByDateCriteria");
        vo.executeQuery();      
        AdfFacesContext.getCurrentInstance().addPartialTarget(timesheetTable);
    }
    
    private TreeItem getTreeItem(String text) {
        for (TreeItem item : root.getChildren()) {
            if (item.getText().equals(text)) {
                return item;
            }
        }
        return null;
    }

    // http://stackoverflow.com/questions/7742293/java-calendar-calculate-start-date-and-end-date-of-the-previous-week
    // http://stackoverflow.com/questions/3023267/java-how-to-calculate-the-first-and-last-day-of-each-week
    
    private Calendar mondayOfLastNWeek(int n) {
        Calendar c = Calendar.getInstance();
        if (n > 0) {
            // last N week
            c.add(Calendar.WEEK_OF_YEAR, -n);
        }
        // first day
        c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        return c;
    }

    public void setTimesheetTable(RichTable timesheetTable) {
        this.timesheetTable = timesheetTable;
    }

    public RichTable getTimesheetTable() {
        return timesheetTable;
    }
}
