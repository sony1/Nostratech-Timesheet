package com.nostratech.timesheet.model;

import com.nostratech.timesheet.model.vo.EmployeeVORowImpl;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.BindingContainer;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;
import oracle.jbo.domain.DBSequence;
import oracle.jbo.server.SequenceImpl;

public class PopupEventListener {
    private BindingContainer bindings;

    public PopupEventListener() {
        bindings = AdfUtils.getBindings();
    }

    public void createNewEmployee(PopupFetchEvent popupFetchEvent) {        
        if (!popupFetchEvent.getLaunchSourceClientId().contains("editLink")) {
            // http://yosrkarker.blogspot.com/2013/09/detail-entity-with-row-key-null-cannot.html
            // http://andrejusb.blogspot.com/2011/11/oracle-adf-11g-table-insert-with-empty.html
            // https://community.oracle.com/thread/1120019?tstart=0
            // https://community.oracle.com/thread/1039879?tstart=0
            // https://community.oracle.com/thread/2127735?tstart=0

            DCIteratorBinding iteratorBinding = null;    
            ViewObject vo = null;
            
            // To avoid error "Detail entity *** cannot find or invalidate its owning entity"
            // row creation follows the order/hierarchy of view objects/links in app module's data model        
            
            // Create new employee row
            iteratorBinding = (DCIteratorBinding)this.bindings.get("EmployeeVO1Iterator");
            vo = iteratorBinding.getViewObject();
            Row newEmployee = iteratorBinding.getRowSetIterator().createRow();
            vo.insertRow(newEmployee); // OR iteratorBinding.getRowSetIterator().insertRow();        
    
            // Create new wages row
            iteratorBinding = (DCIteratorBinding)this.bindings.get("WagesVO1Iterator");
            vo = iteratorBinding.getViewObject();
            Row newWages = vo.createRow(); // OR iteratorBinding.getRowSetIterator().createRow();
            vo.insertRow(newWages);
            
            // Create address row - declarative way
            this.bindings.getOperationBinding("CreateAddress").execute();

            setDialogTitle(popupFetchEvent, "New Employee");
        }
        else {
            setDialogTitle(popupFetchEvent, "Update Employee");
        }
    }    

    public void createNewEmployeeDialogListener(DialogEvent dialogEvent) {
        genericDialogListener(dialogEvent);
    }

    public void createClient(PopupFetchEvent popupFetchEvent) {
        if (!popupFetchEvent.getLaunchSourceClientId().contains("editLink")) {
            bindings.getOperationBinding("CreateClient").execute();            
            setDialogTitle(popupFetchEvent, "New Client");
        }
        else {
            setDialogTitle(popupFetchEvent, "Update Client");
        }
    }    

    public void genericDialogListener(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equalsIgnoreCase("ok")) {
            commit();   
        } else if (dialogEvent.getOutcome().name().equalsIgnoreCase("cancel")) {
            rollback();
        }
    }

    public void popupCanceled(PopupCanceledEvent popupCanceledEvent) {
        rollback();
    }

    private void setDialogTitle(PopupFetchEvent popupFetchEvent, String title) {
        try {
            RichDialog d = (RichDialog)popupFetchEvent.getComponent().getChildren().get(0);
            d.setTitle(title);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }    
    
    private void commit() {
        OperationBinding commit = bindings.getOperationBinding("Commit");
        if (commit != null) commit.execute();
    }

    private void rollback() {
        OperationBinding rollback = bindings.getOperationBinding("Rollback");
        if (rollback != null) rollback.execute();
    }    
}
