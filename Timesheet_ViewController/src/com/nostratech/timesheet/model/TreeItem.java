package com.nostratech.timesheet.model;

import java.util.List;

// http://yonaweb.be/creating_your_own_treemodel_adf_11g_0

public class TreeItem {
    
    private String text, action;
    private List<TreeItem> children;

    public TreeItem(String text, String action) {
        this.text = text;
        this.action = action;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public void setChildren(List<TreeItem> chidren) {
        this.children = chidren;
    }

    public List<TreeItem> getChildren() {
        return children;
    }
}
