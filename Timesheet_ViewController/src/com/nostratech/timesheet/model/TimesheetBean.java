package com.nostratech.timesheet.model;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.context.AdfFacesContext;

public class TimesheetBean {
    private RichSelectOneChoice timesheetLocationSOC;
    private RichSelectOneChoice adminTimesheetLocationSOC;

    public TimesheetBean() {
    }

    /*
     * Update SelectOneChoice for Location when Client is selected
     * Cannot use partial triggers on the Location's SOC, it will trigger validation error because Required = true
     *
     * http://oralublog.wordpress.com/2013/11/24/adf-tip-cascading-dependent-lov-with-mandatory-required-fields/
     * http://adfpractice-fedor.blogspot.com/2012/03/update-model-in-valuechangelistener.html
     */
    public void onClientSelected(ValueChangeEvent valueChangeEvent) {                
        // This step actually invokes Update Model phase for this component
        valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
        
        // Refresh dependent SOC
        // this bean binds controls in two pages (/timesheet.jspx and /admin/timesheet.jspx)
        // therefore we need to check which control is currently bound
        if (timesheetLocationSOC != null) {            
            render(timesheetLocationSOC);
        }
        if (adminTimesheetLocationSOC != null) {
            render(adminTimesheetLocationSOC);
        }
        
        // Jump to the Render Response phase in order to avoid the validation
        FacesContext.getCurrentInstance().renderResponse();
    }
    
    private void render(UIComponent c) {
        AdfFacesContext.getCurrentInstance().addPartialTarget(c);
    }

    public void setTimesheetLocationSOC(RichSelectOneChoice timesheetLocationSOC) {
        this.timesheetLocationSOC = timesheetLocationSOC;
    }

    public RichSelectOneChoice getTimesheetLocationSOC() {
        return timesheetLocationSOC;
    }

    public void setAdminTimesheetLocationSOC(RichSelectOneChoice adminTimesheetLocationSOC) {
        this.adminTimesheetLocationSOC = adminTimesheetLocationSOC;
    }

    public RichSelectOneChoice getAdminTimesheetLocationSOC() {
        return adminTimesheetLocationSOC;
    }
}
