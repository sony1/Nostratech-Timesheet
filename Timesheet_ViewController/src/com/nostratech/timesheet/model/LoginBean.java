package com.nostratech.timesheet.model;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.security.auth.login.LoginException;

import javax.servlet.http.HttpServletRequest;

import weblogic.security.URLCallbackHandler;
import weblogic.security.services.Authentication;
import javax.security.auth.Subject;

import javax.security.auth.login.FailedLoginException;

import javax.servlet.ServletResponse;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import weblogic.servlet.security.ServletAuthentication;

public class LoginBean {
    
    private final Logger LOGGER = Logger.getLogger(LoginBean.class);
    private String uname, passwd;

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUname() {
        return uname;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getPasswd() {
        return passwd;
    }
    
    public String doLogin() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest)ctx.getExternalContext().getRequest();

        try {
            Subject subject = Authentication.login(new URLCallbackHandler(uname, passwd.getBytes())); 
            ServletAuthentication.runAs(subject, req);
            ServletAuthentication.generateNewSessionID(req);
            HttpServletResponse resp = (HttpServletResponse)ctx.getExternalContext().getResponse();
            req.getRequestDispatcher("/adfAuthentication?success_url=/login_success.jsp").forward(req, resp);
        } catch (FailedLoginException e) {
            LOGGER.info(String.format("\n*** LOGIN FAILED - Username: %s ***", uname));
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "LOGIN FAILED", "Invalid username or password");
            ctx.addMessage(null, msg);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
    
    public String getLogSuccessfulLogin() {
        LOGGER.info(String.format("*** LOGIN SUCCESS - User: %s ***", AdfUtils.getCurrentUserLoginName()));
        return null;
    }
}
