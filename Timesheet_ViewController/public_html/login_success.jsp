<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Redirecting...</title>
  </head>  
  <body>
    <jsp:useBean id="loginBean" class="com.nostratech.timesheet.model.LoginBean" />
    <c:if test="${not sessionScope.loggedIn}">
        <jsp:getProperty name="loginBean" property="logSuccessfulLogin" />
        <c:set var="loggedIn" value="1" scope="session" />
    </c:if>
    <c:redirect url="/faces/timesheet" />
  </body>
</html>